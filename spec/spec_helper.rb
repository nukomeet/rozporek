require 'rack/test'
require 'rspec'
require 'capybara/dsl'

ENV['RACK_ENV'] = 'test'

require_relative "../lib/rozporek"

Capybara.app = Rozporek::Web

module RSpecMixin
  include Capybara::DSL

  def app
    Rozporek::Web
  end
end

RSpec.configure do |c|
  c.include RSpecMixin
  c.include Rack::Test::Methods
end
