require 'spec_helper'

RSpec.describe Rozporek do
  before do
    `rm -rf #{app.root}/public/test*`
  end

  describe '::dirs' do
    let!(:file) { `mkdir #{app.root}/public/test-foo` }

    it 'returns list of all directories in public/' do
      expect(Rozporek.dirs).to eq(['test-foo'])
    end
  end
end
