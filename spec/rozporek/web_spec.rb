require 'spec_helper'

RSpec.describe Rozporek::Web do
  before do
    `rm -rf #{app.root}/public/test*`
  end

  it 'Uploading a zip file without specifying dirname' do
    visit '/'

    attach_file 'file', "#{Rozporek::Web.root}/spec/file.zip"
    click_button 'Submit'

    expect(page).to have_text 'Success'

    files = Dir['public/**/*'].map{ |f| f[/public\/(.+)/, 1] }
    expect(files).to include("test/foo")
    expect(files).to include("test/foo/bar.txt")
  end

  it 'Uploading a zip file and specifying dirname' do
    visit '/'

    fill_in 'dirname', with: 'baz'
    attach_file 'file', "#{Rozporek::Web.root}/spec/file.zip"
    click_button 'Submit'

    expect(page).to have_text 'Success'

    files = Dir['public/**/*'].map{ |f| f[/public\/(.+)/, 1] }
    expect(files).to include("testbaz/foo")
    expect(files).to include("testbaz/foo/bar.txt")
  end
end
