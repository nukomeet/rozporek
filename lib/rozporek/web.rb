require 'erb'
require 'sinatra/base'
require 'rack-flash'

require_relative './extract'

module Rozporek
  class Web < Sinatra::Base
    enable :sessions

    use Rack::Flash

    set :root, File.expand_path(File.dirname(__FILE__) + "/../..")
    set :views, "#{root}/web/views"

    get('/') do
      erb :index
    end

    post('/') do
      if params[:file][:type] == 'application/zip'
        Rozporek::Extract.call(params[:file], params[:dirname], self.class.root)
        flash[:notice] = 'Success'
      else
        flash[:error] = 'It should be a zip file'
      end

      redirect '/'
    end
  end
end

if defined?(::ActionDispatch::Request::Session) &&
   !::ActionDispatch::Request::Session.respond_to?(:each)
  # mperham/sidekiq#2460
  # Rack apps can't reuse the Rails session store without
  # this monkeypatch
  class ActionDispatch::Request::Session
    def each(&block)
      hash = self.to_hash
      hash.each(&block)
    end
  end
end
