require 'zip'

module Rozporek
  module Extract
    def call(file_hash, dest, root)
      Zip::File.open(file_hash[:tempfile].path) do |zip_file|
        zip_file.each do |f|
          f_path = File.join("#{root}/public/#{env_prefix}#{dest}", f.name)
          FileUtils.mkdir_p(File.dirname(f_path))
          zip_file.extract(f, f_path)
        end
      end
    end

    def env_prefix
      if ENV['RACK_ENV'] == 'test'
        'test'
      else
        ''
      end
    end

    extend self
  end
end
