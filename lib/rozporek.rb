require "rozporek/version"
require "rozporek/web"
require 'rozporek/rails'

module Rozporek
  module Methods
    def dirs
      Dir.entries("#{Rozporek::Web.root}/public").delete_if do |file|
        file == 'index.erb' || file == '.' || file == '..'
      end
    end
  end

  extend Methods
end
