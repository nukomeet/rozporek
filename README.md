# Local

A Sinatra app for uploading and extracting the zip files

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rozporek', github: 'asok/local'
```

And then execute:

    $ bundle

## Usage

### Rails application

#### Setup backend

#### Mount

Add this to the routes:

```rb
mount Local::Web => '/rozporek', :as => 'rozporek'
```

## Testing

`bundle exec rspec`

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/asok/local.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

